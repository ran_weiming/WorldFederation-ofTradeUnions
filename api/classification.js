import {
	apipost
} from '@/utils/request.js'
export function list(data){
	return apipost({
		url:'shop/goods/category',
		data:data
	})
}
export function mallList(data){
	return apipost({
		url:'shop/goods',
		data:data
	})
}
export function detail(data){
	return apipost({
		url:'shop/goods/getGoodsDetail',
		data:data
	})
}