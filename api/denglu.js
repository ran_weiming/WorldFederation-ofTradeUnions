import {
	apipost
} from '@/utils/request.js'
export function send_message(data){
	return apipost({
		url:'common/send_message',
		data:data
	})
}
export function mobile_login(data){
	return apipost({
		url:'user/mobile_login',
		data:data
	})
}