import {
	apipost
} from '@/utils/request.js'
export function set_info(data){
	return apipost({
		url:'user/set_info',
		data:data
	})
}