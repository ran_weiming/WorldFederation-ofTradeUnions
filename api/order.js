import {
	apipost
} from '@/utils/request.js'
export function order(data){
	return apipost({
		url:'order',
		data:data
	})
}
export function download_document(data){
	return apipost({
		url:'order/download_document',
		data:data
	})
}
export function cancel(data){
	return apipost({
		url:'order/cancel',
		data:data
	})
}
export function replacement_order(data){
	return apipost({
		url:'replacement_order',
		data:data
	})
}
export function cancelorder(data){
	return apipost({
		url:'replacement_order/cancel',
		data:data
	})
}
export function download_document1(data){
	return apipost({
		url:'replacement_order/download_document',
		data:data
	})
}