import {
	apipost
} from '@/utils/request.js'
export function privacy_agreement(data){
	return apipost({
		url:'decorate/privacy_agreement',
		data:data
	})
}
export function customer_service_agreement(data){
	return apipost({
		url:'decorate/customer_service_agreement',
		data:data
	})
}
export function contract_example(data){
	return apipost({
		url:'decorate/contract_example',
		data:data
	})
}