import {
	apipost
} from '@/utils/request.js'
export function detail(data){
	return apipost({
		url:'decorate/project/house/gift/detail',
		data:data
	})
}
export function configuregift(data){
	return apipost({
		url:'order/configure_gift',
		data:data
	})
}
export function configure_replacement(data){
	return apipost({
		url:'order/configure_replacement',
		data:data
	})
}
export function replacement(data){
	return apipost({
		url:'replacement_order/detail',
		data:data
	})
}
export function replacement_orderpay(data){
	return apipost({
		url:'replacement_order/pay',
		data:data
	})
}
export function sign_replacement_contract(data){
	return apipost({
		url:'replacement_order/sign_replacement_contract',
		data:data
	})
}
export function load_package(data){
	return apipost({
		url:'decorate/project/house/gift/load_package',
		data:data
	})
}
export function configure_load_package(data){
	return apipost({
		url:'order/configure_load_package',
		data:data
	})
}