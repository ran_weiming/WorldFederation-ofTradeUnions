import {
	apipost
} from '@/utils/request.js'
export function banner(data){
	return apipost({
		url:'cms/banner',
		data:data
	})
}
export function notice(data){
	return apipost({
		url:'cms/notice',
		data:data
	})
}
export function company_case(data){
	return apipost({
		url:'cms/company_case',
		data:data
	})
}