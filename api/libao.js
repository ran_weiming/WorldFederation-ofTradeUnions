import {
	apipost
} from '@/utils/request.js'
export function project(data){
	return apipost({
		url:'decorate/project',
		data:data
	})
}