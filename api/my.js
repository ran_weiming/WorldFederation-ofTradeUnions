import {
	apipost
} from '@/utils/request.js'
export function info(data){
	return apipost({
		url:'user/info',
		data:data
	})
}
export function person_auth(data){
	return apipost({
		url:'user/person_auth',
		data:data
	})
}