import config from './config.js'

const hostname = config.https;

export function getHostName() {
	return hostname
}

export function uploadImage(image) {
	const token = uni.getStorageSync('token');
	return new Promise((relove, reject) => {
		uni.uploadFile({
			url: hostname + 'common/upload_image',
			method: 'post',
			filePath: image,
			name: 'image',
			header: {
				'Authorization': 'Bearer '+token
			},
			success(res) {
				if (res.data.status === 500) {
					uni.showToast({
						title: '您未登录',
						icon: 'none'
					});
					uni.setStorageSync('token','')
					setTimeout(() => {
						uni.navigateTo({
							url: '/pages/denglu/denglu'
						})
					}, 1500);
				}
				if (res.data.status === 500) {
					uni.showToast({
						title: '您未登录',
						icon: 'none'
					});
					setTimeout(() => {
						uni.navigateTo({
							url: '/pages/denglu/denglu'
						})
					}, 1500);
				}
				relove(JSON.parse(res.data))
			},
			fail(error) {
				reject(error)
			}
		})
	})
}

export function apipost(params) {
	const token = uni.getStorageSync('token');
	return new Promise((relove, reject) => {
		uni.request({
			url: hostname + params.url,
			method: 'post',
			data: params.data,
			header: {
				'Authorization': 'Bearer '+token
			},
			success(res) {
				if (res.data.status === 500) {
					uni.showToast({
						title: '您未登录',
						icon: 'none'
					})
					uni.setStorageSync('token','')
					setTimeout(res => {
						uni.navigateTo({
							url: '/pages/denglu/denglu'
						})
					}, 2000)
				}
				if (res.data.status === 500) {
					uni.showToast({
						title: res.data.msg,
						icon: 'none'
					})
					setTimeout(re => {
						uni.navigateTo({
							url: '/pages/denglu/denglu'
						})
					}, 2000)
				}
				relove(res.data)
			},
			fail(error) {
				reject(error)
			}
		})
	})
}